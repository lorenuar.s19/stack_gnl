/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_bonus.h                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/03 12:15:17 by lorenuar          #+#    #+#             */
/*   Updated: 2020/05/26 10:21:16 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_BONUS_H
# define GET_NEXT_LINE_BONUS_H

/*
** ***** **
** MACRO **
** ***** **
*/
# ifndef BUFFER_SIZE
#  define BUFFER_SIZE 2048
# endif

/*
** ******* **
** INCLUDE **
** ******* **
*/
# include <unistd.h>
# include <stdlib.h>
# include <stdio.h>
# include <limits.h>

/*
** ********* **
** FUNCTIONS **
** ********* **
*/

int			get_next_line(int fd, char **line);
size_t		hasto(char *s, char c);
char		*jointo(char *s1, char *s2, char **tofree);

#endif
